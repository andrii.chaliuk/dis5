import java.sql.*;

public class MasterMaster {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");

        String masterUrl = "jdbc:postgresql://localhost:15433/mydb";
        String slaveUrl = "jdbc:postgresql://localhost:25433/mydb";
        String slave2Url = "jdbc:postgresql://localhost:35433/mydb";

        String userWrite = "admin";
        String passwordWrite = "testpass";

        try {
            Connection masterConnection = DriverManager.getConnection(masterUrl, userWrite, passwordWrite);
            Statement masterStatement = masterConnection.createStatement();

            Connection slaveConnection = DriverManager.getConnection(slaveUrl, userWrite, passwordWrite);
            Statement slaveStatement = slaveConnection.createStatement();

            Connection slave2Connection = DriverManager.getConnection(slave2Url, userWrite, passwordWrite);
            Statement slave2Statement = slaveConnection.createStatement();

            // Master Create Table
            String createQuery = "CREATE TABLE \"shop\" (\"id\" integer PRIMARY KEY,\"name\" varchar(255) NOT NULL,\"address\" text NOT NULL);\n";
            try {
                masterStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            String insertQuery = "INSERT INTO shop (id, name, address) VALUES ('1', 'shop1', 'address1')";

            // Slave Insert Query
            try {
                slaveStatement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // already have record with key 1
            }

            Thread.sleep(1000);

            String selectQuery = "SELECT id, name FROM shop";

            // Slave2 Select Query
            ResultSet resultSet = slave2Statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }

            Thread.sleep(1000);

            // Slave2 Drop Query
            String dropQuery = "DROP TABLE shop";
            try {
                slave2Statement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table doesn`t exist
            }

            Thread.sleep(1000);

            System.out.println("Master Create,\n Slave Insert,\n Slave2 Select and drop\n\n");

            // TEST
            //______________________________________

            // Slave Create
            try {
                slaveStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }
            Thread.sleep(1000);

            // ________________________________________

            // Slave2 Insert
            try {
                slave2Statement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace();
            }
            Thread.sleep(1000);

            // Slave Select Query
            resultSet = slaveStatement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }
            Thread.sleep(1000);

            resultSet = slave2Statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }
            Thread.sleep(1000);

            resultSet = masterStatement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }
            Thread.sleep(1000);

            // Master Drop
            try {
                masterStatement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace();
            }
            Thread.sleep(1000);

            System.out.println("Slave create,\n slave2 insert,\n master, slave, slave2 select,\n master drop\n\n");

            // __________________________________________
            // Master cycle
            // __________________________________________

            // Master Create Table
            try {
                masterStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            Thread.sleep(1000);

            // Master Insert Query
            try {
                masterStatement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // already have record with key 1
            }

            Thread.sleep(1000);

            // Master Select Query
            resultSet = masterStatement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }

            Thread.sleep(1000);

            // Master Drop Query
            try {
                masterStatement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table doesn`t exist
            }

            System.out.println("Master cycle");

            // __________________________________________
            // Slave cycle
            // ___________________________________________

            // Slave Create Table
            try {
                slaveStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            Thread.sleep(1000);

            // Slave Insert Query
            try {
                slaveStatement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // already have record with key 1
            }

            Thread.sleep(1000);

            // Slave Select Query
            resultSet = slaveStatement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }

            Thread.sleep(1000);

            // Slave Drop Query
            try {
                slaveStatement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table doesn`t exist
            }

            System.out.println("Slave cycle");

            Thread.sleep(1000);

            // __________________________________________
            // Slave2 cycle
            // __________________________________________

            // Slave2 Create Table
            try {
                slave2Statement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            Thread.sleep(1000);

            // Slave2 Insert Query
            try {
                slave2Statement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // already have record with key 1
            }

            Thread.sleep(1000);

            // Slave2 Select Query
            resultSet = slave2Statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }

            Thread.sleep(1000);

            // Slave2 Drop Query
            try {
                slave2Statement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table doesn`t exist
            }

            System.out.println("Slave2 cycle");

            Thread.sleep(1000);


            masterStatement.close();
            masterConnection.close();
            slaveStatement.close();
            slaveConnection.close();
            slave2Statement.close();
            slave2Connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
