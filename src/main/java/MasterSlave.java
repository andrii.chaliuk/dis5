import java.sql.*;

public class MasterSlave {
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");

        String masterUrl = "jdbc:postgresql://localhost:14432/my_database";
        String slaveUrl = "jdbc:postgresql://localhost:5433/my_database";

        String userWrite = "reading_user";
        String passwordWrite = "reading_pass";

        try {
            Connection masterConnection = DriverManager.getConnection(masterUrl, userWrite, passwordWrite);
            Statement masterStatement = masterConnection.createStatement();

            Connection slaveConnection = DriverManager.getConnection(slaveUrl, userWrite, passwordWrite);
            Statement slaveStatement = slaveConnection.createStatement();

            // Master Create Table
            String createQuery = "CREATE TABLE \"shop\" (\"id\" integer PRIMARY KEY,\"name\" varchar(255) NOT NULL,\"address\" text NOT NULL);\n";
            try {
                masterStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            String insertQuery = "INSERT INTO shop (id, name, address) VALUES ('1', 'shop1', 'address1')";

            // Master Insert Query
            try {
                masterStatement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // already have record with key 1
            }

            String selectQuery = "SELECT id, name FROM shop";

            // Slave Select Query
            ResultSet resultSet = slaveStatement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }

            // Master Drop Query
            String dropQuery = "DROP TABLE shop";
            try {
                masterStatement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table doesn`t exist
            }

            // TEST
            //______________________________________

            // Slave Create
            try {
                slaveStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            // ________________________________________

            // Master Create
            try {
                masterStatement.executeUpdate(createQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table already exist
            }

            // Slave Insert
            try {
                slaveStatement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace();
            }

            // Slave Drop
            try {
                slaveStatement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace();
            }

            // Master Insert Query
            try {
                masterStatement.executeUpdate(insertQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // already have record with key 1
            }

            // Master Select
            resultSet = masterStatement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                System.out.println(id + " " + name);
            }

            // Master Drop
            try {
                masterStatement.executeUpdate(dropQuery);
            } catch (org.postgresql.util.PSQLException e) {
                e.printStackTrace(); // table doesn`t exist
            }

            masterStatement.close();
            masterConnection.close();
            slaveStatement.close();
            slaveConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
